FROM debian

RUN apt update && apt upgrade -y
RUN apt install mariadb-client awscli -y

COPY . /

WORKDIR /
ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
